package com.casoftware.dao;

import java.util.List;

import com.casoftware.modal.User;

public interface MainDao extends Dao {

	List<User> getAllUsers();

}
